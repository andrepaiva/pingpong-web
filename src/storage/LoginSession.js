import {history, storage} from "App";

class LoginSession {

  constructor() {
    this.logged = this.get();
  }

  get = () => {
    this.logged = this.logged || storage.get("logged") || {};
    return this.logged;
  };

  save = (logged) => {
    this.logged = {
      ...logged,
      expiresAt: Date.now() + (logged.expiration * 1000)
    };
    storage.set("logged", this.logged);
    setTimeout(this.logout, this.expiresAt() - Date.now());
  };

  clear = () => {
    storage.remove("logged");
    this.logged = {};
  };

  logout = () => history.push("/logout");

  isLogged = () => {
    const isLogged = Date.now() < (this.expiresAt() || 0);
    if (!isLogged) this.logout();
    return isLogged;
  };

  id = () => this.get().id;
  expiresAt = () => this.get().expiresAt;

}

export default LoginSession;
