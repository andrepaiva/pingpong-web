// @material-ui components
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import withStyles from "@material-ui/core/styles/withStyles";
import EmailIcon from "@material-ui/icons/Email";
// core components
import Component from "component/Component";
import PasswordInput from "component/input/PasswordInput";
import TextInput from "component/input/TextInput";
import {LOGIN} from "graphql/user";
// resources
import React from "react";
import {Link} from "react-router-dom";
import style from "resources/jss/page/auth/loginCard";

class LoginCard extends Component {

  initDefault() {
    return {
      email: "",
      password: "",
      isError: false
    };
  }

  loginUser = e => {
    e.preventDefault();
    if (this.state.email.trim() === "" || this.state.password.trim() === "") {
      this.setState({isError: true});
      return;
    }
    const {isError, ...data} = this.state;
    this.query(LOGIN, {data}).then(({data, errors}) => {
      const isError = !!errors;
      this.setState({isError});
      if (!isError) {
        this.loginSession.save(data.login);
        this.reset();
        this.history.push("/rank");
      }
    });
  };

  render() {
    const {classes} = this.props;
    return (
      <Card className={classes.card}>
        <h3>{this.msg("login")}</h3>
        <CardContent className={classes.content}>
          <TextInput id={"email"} icon={EmailIcon} value={this.state.email} onChange={this.onChange}/>
          <PasswordInput value={this.state.password} onChange={this.onChange}/>
        </CardContent>
        <div>
          {this.state.isError ? <small className={classes.error}>{this.msg("auth.invalid")}</small> : null}
          <Button color={"primary"} className={classes.button} onClick={this.loginUser} variant="contained">
            {this.msg("login")}
          </Button>
          <div>
            {this.msg("login.new")}
            <Link to={"/signUp"}>{this.msg("signup")}</Link>
          </div>
        </div>
      </Card>
    );
  }
}

export default withStyles(style)(LoginCard);
