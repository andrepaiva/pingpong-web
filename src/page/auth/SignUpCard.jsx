// @material-ui components
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import withStyles from "@material-ui/core/styles/withStyles";
import EmailIcon from "@material-ui/icons/Email";
import PersonIcon from "@material-ui/icons/Person";
// core components
import Component from "component/Component";
import PasswordInput from "component/input/PasswordInput";
import TextInput from "component/input/TextInput";
import {SIGN_UP} from "graphql/user";
// resources
import React from "react";
import {Link} from "react-router-dom";
import style from "resources/jss/page/auth/signUpCard";

class SignUpCard extends Component {

  initDefault() {
    return {
      name: "",
      email: "",
      password: "",
      isError: false
    };
  }

  signUpUser = e => {
    e.preventDefault();
    if (this.state.name.trim() === "" || this.state.email.trim() === "" || this.state.password.trim() === "") {
      this.setState({isError: true});
      return;
    }
    const {isError, ...data} = this.state;
    this.mutate(SIGN_UP, {data}).then(({data, errors}) => {
      const isError = !!errors;
      this.setState({isError});
      if (!isError) {
        this.loginSession.save(data.signUp);
        this.reset();
        this.history.push("/rank");
      }
    });
  };

  render() {
    const {classes} = this.props;
    return (
      <Card className={classes.card}>
        <h3>{this.msg("signup")}</h3>
        <CardContent className={classes.content}>
          <TextInput id={"name"} value={this.state.name} icon={PersonIcon} onChange={this.onChange}/>
          <TextInput id={"email"} value={this.state.email} icon={EmailIcon} onChange={this.onChange}/>
          <PasswordInput value={this.state.password} onChange={this.onChange}/>
        </CardContent>
        <div>
          {this.state.isError ? <small className={classes.error}>{this.msg("auth.invalid")}</small> : null}
          <Button color={"primary"} className={classes.button} onClick={this.signUpUser} variant="contained">
            {this.msg("signup")}
          </Button>
          <div>
            {this.msg("signup.exist")}
            <Link to={"/login"}>{this.msg("login")}</Link>
          </div>
        </div>
      </Card>
    );
  }
}

export default withStyles(style)(SignUpCard);
