// @material-ui components
import Button from "@material-ui/core/Button";
import Paper from '@material-ui/core/Paper';
import withStyles from "@material-ui/core/styles/withStyles";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
// core components
import Component from "component/Component";
import GridContainer from "component/grid/GridContainer";
import GridItem from "component/grid/GridItem";
import {PLAYERS} from "graphql/game";
// resources
import React from 'react';
import style from "resources/jss/page/game/rank"

const columns = [
  {id: 'name', label: 'Name', minWidth: 200},
  {id: 'wins', label: 'Wins', minWidth: 100},
  {id: 'losses', label: 'Losses', minWidth: 100},
  {id: 'total', label: 'Total Games', minWidth: 100},
];

class Rank extends Component {

  constructor(props) {
    super(props);
    this.initPlayers();
  }

  initDefault() {
    return {
      page: 0,
      rowsPerPage: 5,
      players: []
    };
  }

  initPlayers() {
    this.query(PLAYERS, {accessToken: this.loginSession.get().accessToken}).then(({data, errors}) => {
        if (!errors) {
          const players = data.players.sort((a, b) => b.wins - a.wins || a.losses - b.losses);
          this.setState({players})
        }
      }
    );
  }

  createData = ({wins, losses, ...data}) => ({...data, wins, losses, total: wins + losses});

  handleChangePage = (event, newPage) => {
    this.setState({page: newPage});
  };

  handleChangeRowsPerPage = (event) => {
    this.setState({rowsPerPage: event.target.value, page: 0});
  };

  render() {
    const {page, rowsPerPage, players} = this.state;
    const {classes} = this.props;
    return (
      <GridContainer justify="center">
        <h1 className={classes.rank}>
          {this.msg("rank")}
        </h1>
        <GridItem>
          <Paper className={classes.root}>
            <div className={classes.tableWrapper}>
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    {columns.map(column => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{minWidth: column.minWidth}}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {players.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(player => {
                    const p = this.createData(player);
                    return (
                      <TableRow hover role="checkbox" tabIndex={-1} key={p.uuid}>
                        {columns.map(column => (
                          <TableCell key={column.id} align={column.align}>
                            {p[column.id]}
                          </TableCell>
                        ))}
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </div>
            <TablePagination
              rowsPerPageOptions={[5, 10, 20]}
              component="div"
              count={players.length}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                'aria-label': 'previous page',
              }}
              nextIconButtonProps={{
                'aria-label': 'next page',
              }}
              onChangePage={this.handleChangePage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
            />
          </Paper>
        </GridItem>
        <GridItem>
          <Button size={"large"} color={"primary"} className={classes.button} onClick={() => this.history.push("/game/new")} variant={"contained"}>
            {this.msg("new.game")}
          </Button>
        </GridItem>
      </GridContainer>
    );
  }
}

export default withStyles(style)(Rank);
