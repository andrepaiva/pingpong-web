// @material-ui components
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Component from "component/Component";
import GridContainer from "component/grid/GridContainer";
import GridItem from "component/grid/GridItem";
import {CREATE_GAME} from "graphql/game";
import Score from "page/game/Score";
// resources
import React from "react";
import style from "resources/jss/page/game/game";

class Game extends Component {

  initDefault() {
    if (!this.history.location.state) this.history.push("/game/new");
    const players = this.history.location.state || {};
    return {
      score1: {player: players.p1, score: 0},
      score2: {player: players.p2, score: 0},
      winner: "",
      isEnd: false
    };
  }

  score = (s, i) => {
    const key = i === 1 ? "score1" : "score2";
    const score = s.score + 1;
    this.setState({
      [key]: {...s, score},
    }, () => this.update());
  };

  update = () => {
    const s1 = this.state.score1;
    const s2 = this.state.score2;
    let winner = "";

    if (s1.score === 11) {
      winner = s1.player.name;
    } else if (s2.score === 11) {
      winner = s2.player.name;
    }
    if (winner) {
      this.setState({winner, isEnd: true}, () => this.end());
    }
  };

  end = () => {
    const score1 = this.state.score1;
    const score2 = this.state.score2;
    if (score1 && score2) {
      this.mutate(CREATE_GAME, {
        accessToken: this.loginSession.get().accessToken,
        data: {
          score1: {
            player: {uuid: score1.player.uuid},
            score: score1.score
          },
          score2: {
            player: {uuid: score2.player.uuid},
            score: score2.score
          }
        }
      });
    }
  };

  finish = e => {
    e.preventDefault();
    this.reset();
    this.history.push("/rank");
  };

  render() {
    const {classes} = this.props;
    return (
      <div>
        <GridContainer justify="center">
          <h1 className={classes.winner}>
            {this.state.isEnd ? `${this.msg("winner")} ${this.state.winner}!` : ""}
          </h1>
        </GridContainer>
        <GridContainer justify="center">
          <GridItem key={'s1'} sm={6}>
            <Score
              state={this.state.score1}
              color={"primary"}
              onScore={() => this.score(this.state.score1, 1)}
              disabled={this.state.isEnd}
            />
          </GridItem>
          <GridItem key={'s2'} sm={6}>
            <Score
              state={this.state.score2}
              color={"secondary"}
              onScore={() => this.score(this.state.score2, 2)}
              disabled={this.state.isEnd}
            />
          </GridItem>
        </GridContainer>
        <GridContainer justify="center">
          <Button disabled={!this.state.isEnd} color={"default"} size={"large"} className={classes.button} onClick={this.finish} variant="contained">
            {this.msg("finish")}
          </Button>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(style)(Game);
