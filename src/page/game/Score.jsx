// @material-ui components
import {CardContent} from "@material-ui/core";
import Button from "@material-ui/core/Button/index";
import Card from "@material-ui/core/Card/index";
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Component from "component/Component";
// resources
import React from "react";
import style from "resources/jss/page/game/score";

class Score extends Component {

  render() {
    const {state, classes, color, onScore, disabled} = this.props;
    return (
      <Card className={classes.card}>
        <CardContent className={classes.content}>
          <h3>{state.player.name}</h3>
          <h2>{state.score}</h2>
          <div>
            <Button disabled={disabled} color={color} className={classes.button} onClick={onScore} variant="contained">
              {this.msg("score")}
            </Button>
          </div>
        </CardContent>
      </Card>
    );
  }
}

export default withStyles(style)(Score);
