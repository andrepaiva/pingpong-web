// @material-ui components
import Button from "@material-ui/core/Button/index";
import Card from "@material-ui/core/Card/index";
import CardContent from "@material-ui/core/CardContent/index";
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Component from "component/Component";
import TextInput from "component/input/TextInput";
import {CREATE_PLAYERS} from "graphql/game";
// resources
import React from "react";
import style from "resources/jss/page/game/newGame";

class NewGame extends Component {

  initDefault() {
    return {
      nameP1: this.msg("nameP1"),
      nameP2: this.msg("nameP2"),
      isError: false
    };
  }

  new = e => {
    e.preventDefault();
    const {nameP1, nameP2} = this.state;
    if (nameP1.trim() === "" || nameP2.trim() === "") {
      this.setState({isError: true});
      return;
    }
    this.mutate(CREATE_PLAYERS, {
      accessToken: this.loginSession.get().accessToken,
      player1: {name: nameP1},
      player2: {name: nameP2}
    }).then(({data, errors}) => {
      if (!errors) {
        this.history.push("/game", data);
      }
    });
  };

  render() {
    const {classes} = this.props;
    return (
      <Card className={classes.card}>
        <h3>{this.msg("new.game")}</h3>
        <CardContent className={classes.content}>
          <TextInput
            id={"nameP1"}
            value={this.state.nameP1}
            onChange={this.onChange}
          />
          <TextInput
            id={"nameP2"}
            value={this.state.nameP2}
            onChange={this.onChange}
          />
        </CardContent>
        <div>
          {this.state.isError ? <small className={classes.error}>{this.msg("player.invalid")}</small> : null}
          <Button color={"primary"} className={classes.button} onClick={this.new} variant="contained">
            {this.msg("start")}
          </Button>
        </div>
      </Card>
    );
  }
}

export default withStyles(style)(NewGame);
