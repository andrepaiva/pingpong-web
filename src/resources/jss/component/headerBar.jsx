const link = {
  marginLeft: "20px",
  "&,&:hover,&:focus": {
    color: "#FFF",
  }
};

const style = {
  bar: {
    color: "#47daff"
  },
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
    ...link,
    fontSize: "30px"
  },
  link,
  name: {
    flexGrow: 0,
    color: "#FFF"
  },
};

export default style;