const style = {
  container: {
    paddingTop: "10vh",
    marginRight: "-15px",
    marginLeft: "-15px",
    textAlign: "center"
  },
  item: {
    position: "relative",
    width: "100%",
    minHeight: "1px",
    paddingRight: "15px",
    paddingLeft: "15px"
  }
};

export default style;