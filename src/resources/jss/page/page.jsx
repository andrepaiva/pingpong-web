import image from "resources/img/bk5.jpg";

const style = {
  container: {
    zIndex: "2",
    width: "100%",
    maxWidth: "900px",
    marginRight: "auto",
    marginLeft: "auto",
    position: "relative"
  },
  pageHeader: {
    minHeight: "100vh",
    maxHeight: "1000px",
    height: "auto",
    display: "inherit",
    position: "relative",
    backgroundImage: `url(${image})`,
    backgroundSize: "cover",
    backgroundPosition: "top center",
    margin: "0",
    padding: "0",
    border: "0",
    alignItems: "center",
  }
};

export default style;
