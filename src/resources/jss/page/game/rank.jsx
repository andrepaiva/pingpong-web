const style = {
  rank: {
    color: "#FFF",
    marginTop: "-100px",
  },
  button: {
    marginTop: "30px",
  },
  root: {
    width: '100%',
  },
  tableWrapper: {
    maxHeight: 407,
    overflow: 'auto',
  },
};

export default style;