import {button, card, content, error} from "resources/jss/component/card";

const style = {
  error,
  card,
  content,
  button
};

export default style;
