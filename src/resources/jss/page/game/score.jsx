import {container, textCenter} from "resources/jss/app";
import {button} from "resources/jss/component/card";

const style = theme => ({
  card: {
    minWidth: "300px",
    borderRadius: "180px",
    height: "300px",
    ...textCenter,
    ...container,
    [theme.breakpoints.down("xs")]: {
      marginTop: "40px",
    }
  },
  content: {
    "marginTop": "20px"
  },
  button
});

export default style;
