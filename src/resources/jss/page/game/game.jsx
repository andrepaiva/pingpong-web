const style = {
  winner: {
    color: "#FFF",
    marginTop: "-100px"
  },
  button: {
    backgroundColor: "#FFF",
    "&:hover": {
      backgroundColor: "#DDD"
    }
  }
};

export default style;
