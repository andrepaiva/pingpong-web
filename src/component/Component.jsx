// core components
import {history, loginSession, storage} from "App";
import graphql from "graphql/GraphQLClient";
// resources
import React from "react";
import intl from "react-intl-universal";

class Component extends React.Component {

  constructor(props) {
    super(props);
    this.graphql = graphql;
    this.history = history;
    this.storage = storage;
    this.loginSession = loginSession;
    this.default = this.initDefault(props);
    this.state = this.getDefault();
  }

  initDefault() {
    return {};
  }

  reset() {
    this.setState(this.getDefault());
  }

  getDefault = key => JSON.parse(JSON.stringify(key ? this.default[key] : this.default));

  onChange = e => {
    e.preventDefault();
    this.setState({[e.target.id]: e.target.value});
  };

  msg = (k) => intl.get(k);

  query(query, variables) {
    return this.graphql.query({
      query: query,
      variables: variables
    })
  }

  mutate(mutation, variables) {
    return this.graphql.mutate({
      mutation: mutation,
      variables: variables
    });
  }

}

export default Component;
