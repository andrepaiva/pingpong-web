// @material-ui component
import AppBar from '@material-ui/core/AppBar';
import withStyles from "@material-ui/core/styles/withStyles";
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
// core component
import Component from "component/Component";
import React from 'react';
import {Link} from "react-router-dom";
import logo from "resources/img/logo.ico"
// resources
import style from "resources/jss/component/headerBar";

class HeaderBar extends Component {

  render() {
    const {classes} = this.props;
    const username = this.loginSession.get().user ? this.loginSession.get().user.name : "";
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <img src={logo} width="32" height="32" alt={"logo"}/>
            <Link to={username ? "/rank" : "/"} className={classes.title}>
              {this.msg("title")}
            </Link>
            <Typography variant="subtitle2" className={classes.name}>
              {username}
            </Typography>
            <a href={"http://localhost:7474"} target={"_blank"} className={classes.link}>
              {this.msg("neo4j")}
            </a>
            {username ?
              <Link to={"/logout"} className={classes.link}>
                {this.msg("logout")}
              </Link> : null}
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default withStyles(style)(HeaderBar);