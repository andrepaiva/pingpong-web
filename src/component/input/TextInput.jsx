// @material-ui components
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
// core components
import Component from "component/Component";
// resources
import React from "react";

class TextInput extends Component {

  render() {
    const {value, onChange, icon, ...props} = this.props;
    const id = this.props.id || "input";
    return (
      <TextField
        id={id}
        placeholder={this.msg(id)}
        type={"text"}
        value={value}
        onChange={onChange}
        fullWidth
        InputProps={{
          startAdornment: icon && (
            <InputAdornment position="start">
              {<this.props.icon/>}
            </InputAdornment>
          )
        }}
        {...props}
      />
    );
  }
}

export default TextInput;
