import gql from 'graphql-tag';

const logged = gql`
  fragment logged on Logged {
    user {
      uuid
      name
      email
    }
    accessToken
    startedAt
    expiration
  }
`;

export const USER = gql`
  query ($accessToken: String!) {
    players(accessToken: $accessToken) {
      uuid
      name
    }
  }
`;

export const SIGN_UP = gql`
  ${logged}
  mutation ($data: SignUp!) {
    signUp(data: $data) {
      ...logged
    }
  }
`;

export const LOGIN = gql`
  ${logged}
  mutation ($data: Login!) {
    login(data: $data) {
      ...logged
    }
  }
`;