import gql from 'graphql-tag';

export const PLAYERS = gql`
  fragment score on Score {
    player {
      name
    }
    score
  }

  fragment game on Game {
    score1 {
      ...score
    }
    score2 {
      ...score
    }
  }

  query ($accessToken: String!) {
    players(accessToken: $accessToken) {
      uuid
      name
      wins
      losses
      wonGames {
        ...game
      }
      lostGames {
        ...game
      }
    }
  }
`;

export const CREATE_PLAYERS = gql`
  mutation ($accessToken: String!, $player1: CreatePlayer!, $player2: CreatePlayer!) {
    p1: createPlayer(accessToken: $accessToken, data: $player1) {
      uuid
      name
    }
    p2: createPlayer(accessToken: $accessToken, data: $player2) {
      uuid
      name
    }
  }
`;

export const CREATE_GAME = gql`
  mutation ($accessToken: String!, $data: CreateGame!) {
    createGame(accessToken: $accessToken, data: $data) {
      uuid
      winner {
        uuid
        name
        wins
        losses
      }
      loser {
        uuid
        name
        wins
        losses
      }
      score1 {
        uuid
        score
      }
      score2 {
        uuid
        score
      }
    }
  }
`;
