import {InMemoryCache} from "apollo-cache-inmemory";
import {ApolloClient} from "apollo-client";
import {ApolloLink} from "apollo-link";
import {onError} from "apollo-link-error";
import {HttpLink} from "apollo-link-http";
import {loginSession} from "App";

const httpLink = new HttpLink({uri: process.env.REACT_APP_GRAPHQL_URI});

const errorLink = onError(({graphQLErrors, networkError}) => {
  let isAuthError = false;
  if (networkError) {
    console.warn(`[Network]: ${networkError}`);
  }
  if (graphQLErrors) {
    graphQLErrors.forEach(e => {
      if (!isAuthError && e.key === "auth.not.authorized") isAuthError = true;
      console.warn(`[GraphQL]: ${e.key}`);
    });
  }
  if (isAuthError) loginSession.logout();
});

const graphql = new ApolloClient({
  link: ApolloLink.from([errorLink, httpLink]),
  defaultOptions: {
    query: {
      fetchPolicy: "no-cache",
      errorPolicy: "all"
    },
    mutate: {
      fetchPolicy: "no-cache",
      errorPolicy: "all"
    }
  },
  cache: new InMemoryCache({
    dataIdFromObject: obj => obj.uuid || null
  })
});

export default graphql;